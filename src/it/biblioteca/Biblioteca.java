package it.biblioteca;

import java.util.ArrayList;
import java.util.List;

public class Biblioteca {

    private List<Libro> libreria;

    public Biblioteca() {
        this.libreria = new ArrayList<Libro>();
    }

    public void aggiungiLibro(Libro libro) {
        this.libreria.add(libro);
    }

    public boolean ricercaLibro(String titolo) {
        for (int i = 0; i < this.libreria.size(); i++) {
            if (this.libreria.get(i).getTitolo().equals(titolo)) {
                return true;
            }
        }
        return false;
    }

    public void stampaElenco() {
        for (int i = 0; i < this.libreria.size(); i++)
            System.out.println("Libro n°" + (i + 1) + ": " + this.libreria.get(i).getTitolo());
    }

    public int numeroDiPagineTotale() {
        int totale = 0;
        for (Libro libro: this.libreria) {
            if (libro instanceof LibroStampato)
                totale += ((LibroStampato) libro).getNumeroPagine();
        }
        return totale;
    }


}
