package it.biblioteca;

public class LibroDigitale extends Libro{

    private String formato;

    public LibroDigitale(String titolo, String autore, int annoPubblicazione, String formato) {
        super(titolo, autore, annoPubblicazione);
        this.formato = formato;
    }

    public String getFormato() {
        return this.formato;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public int getNumeroPagine() {
        return 0;
    }
}
