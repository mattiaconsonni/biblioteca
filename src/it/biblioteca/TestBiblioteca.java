package it.biblioteca;

public class TestBiblioteca {
    public static void main(String[] args) {
        Libro libro = new LibroStampato("La fine dei mondi", "Hughes Grant", 1980, 345);
        Libro libro1 = new LibroStampato("La guerra fredda", "Mike Adams", 1999, 500);
        Libro libro2 = new LibroDigitale("Cercasi casa", "Tommaso Filippanti", 2019, "PDF");
        Libro libro3 = new LibroStampato("Qua la zampa", "Mark Waterfall", 2004, 280);
        Libro libro4 = new LibroDigitale("La spada del destino", "Andrej Sapkowski", 1988, "MOBI");
        Biblioteca biblioteca = new Biblioteca();
        biblioteca.aggiungiLibro(libro);
        biblioteca.aggiungiLibro(libro1);
        biblioteca.aggiungiLibro(libro2);
        biblioteca.aggiungiLibro(libro3);
        biblioteca.aggiungiLibro(libro4);
        biblioteca.stampaElenco();
        //PROVA PER VEDERE SE IL LIBRO C'E'
        if (biblioteca.ricercaLibro("La guerra fredda"))
            System.out.println("Il libro è presente.");
        else
            System.out.println("Il libro non è presente.");
        // PROVA PER VEDERE SE IL LIBRO NON C'E'
        if (biblioteca.ricercaLibro("La guerra calda"))
            System.out.println("Il libro è presente.");
        else
            System.out.println("Il libro non è presente.");
        // TEST NUMERO DI PAGIME TOTALE
        System.out.println("Totale pagine stampate in biblioteca: " + biblioteca.numeroDiPagineTotale());
    }
}
